import React from 'react';
import ReactDOM from 'react-dom';
import './style/style.scss';
const App = () => {
  return (
  <div>Sample react application. Navigate to <a href="/api">API get call</a></div>
  )
}
ReactDOM.render(
  <App />,
  document.querySelector('#root')
);