import express from 'express';
import Config from './src/Config.js';

import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackConfig from './webpack.config.js';

const app = express();
app.use(webpackMiddleware(webpack(webpackConfig)));

app.get("/api",(req,res) => {
    res.send({test:'test'});
});

app.listen(Config.port, () => {
    console.log('Listening');
});

//=== kill handler

function ServerShutdown(sig) {
    console.log(`*** KILLED with ${sig} ***`);
    process.kill(process.pid, 'SIGUSR2');
    process.exit(0);
};

process.once('SIGUSR2', () => ServerShutdown('SIGUSR2'));
process.once('SIGTERM', () => ServerShutdown('SIGTERM'));
process.once('SIGHUP', () => ServerShutdown('SIGHUP'));
process.once('SIGINT', () => ServerShutdown('SIGINT'));

setTimeout(() => {
    console.log(`\nserver is running @ http://127.0.0.1:${Config.port}\n`);
}, 2000);
